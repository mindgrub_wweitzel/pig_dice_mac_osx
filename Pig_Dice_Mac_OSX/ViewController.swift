//
//  ViewController.swift
//  Pig_Dice_Mac_OSX
//
//  Created by Wesley Weitzel on 7/18/16.
//  Copyright © 2016 Wesley Weitzel. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    
    @IBOutlet weak var diceImageView: NSImageView!
    
    @IBOutlet weak var turnTotalLabel: NSTextField!
    @IBOutlet weak var playerScoreLabel: NSTextField!
    @IBOutlet weak var computerScoreLabel: NSTextField!
    
    @IBOutlet weak var holdButton: NSButton!
    @IBOutlet weak var rollButton: NSButton!
    @IBOutlet weak var newGameButton: NSButton!
    @IBOutlet weak var viewScoresButton: NSButton!
    
    private let badRollValue = 1
    private let holdValueChance = 5
    
    private var logic = Logic()
    private var scores = ScoresHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func touchRoll(sender: NSButton) {
        roll()
    }
    
    @IBAction func touchHold(sender: NSButton) {
        hold()
    }
    
    @IBAction func touchNewGame(sender: NSButton) {
        reset()
    }
    
    private func roll() {
        let rollValue = Int(arc4random_uniform(6) + 1)
        setImage(rollValue)
        if (rollValue == badRollValue) {
            setTurnTotal(0)
            changeTurn()
        }
        else {
            setTurnTotal(logic.getTurnTotal() + rollValue)
        }
    }
    
    private func hold() {
        setPlayerScore()
        setTurnTotal(0)
        if (logic.getPlayerScore() >= logic.getGoalScore()) {
            endGame()
        } else {
            changeTurn()
        }
    }
    
    private func computerHold() {
        setComputerScore()
        setTurnTotal(0)
        if (logic.getComputerScore() >= logic.getGoalScore()) {
            endGame()
        }
        logic.setIsPlayerTurn(true)
        setButtonStates()
    }
        
    private func computerTurn() {
        let priority = QOS_CLASS_USER_INITIATED
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            NSThread.sleepForTimeInterval(0.75)
            while (!self.logic.getIsPlayerTurn()) {
                let holdValue = Int(arc4random_uniform(UInt32(self.holdValueChance)))
                if (!(self.logic.getComputerScore() + self.logic.getTurnTotal() >= self.logic.getGoalScore()) && (holdValue != 1)) {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.roll()
                    }
                } else {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.computerHold()
                    }
                }
                NSThread.sleepForTimeInterval(0.75)
            }
        }
    }
    
    private func endGame() {
        var message: String = ""
        if (!logic.getIsPlayerTurn()) {
            message += NSLocalizedString("Computer wins", comment: "") + String(logic.getComputerScore()) + NSLocalizedString("to", comment: "") + String(logic.getPlayerScore())
        } else {
            message += logic.getPlayerName() + NSLocalizedString("wins", comment: "") + String(logic.getPlayerScore()) + NSLocalizedString("to", comment: "") + String(logic.getComputerScore())
        }
        dialogOK(message, text: "Press ok to continue")
        let pScore = String(logic.getPlayerScore())
        let cScore = String(logic.getComputerScore())
        scores.saveItem(pScore, compScore: cScore)
        logic.setIsPlayerTurn(true)
        reset()
    }

    func dialogOK(message: String, text: String) -> Bool {
        let myPopup: NSAlert = NSAlert()
        myPopup.messageText = message
        myPopup.informativeText = text //click ok to continue
        myPopup.alertStyle = NSAlertStyle.InformationalAlertStyle
        myPopup.addButtonWithTitle("OK")
        let res = myPopup.runModal()
        if res == NSAlertFirstButtonReturn {
            return true
        }
        return false
    }
    
    private func setTurnTotal(value: Int) {
        logic.setTurnTotal(value)
        turnTotalLabel.stringValue = String(NSLocalizedString("Turn Total : ", comment: "") + String(logic.getTurnTotal()))
    }
    
    private func setPlayerScore() {
        logic.setPlayerScore(logic.getTurnTotal() + logic.getPlayerScore())
        playerScoreLabel.stringValue = String(logic.getPlayerName() + NSLocalizedString(" Score : ", comment: "") + String(logic.getPlayerScore()))
    }
    
    private func setComputerScore() {
        logic.setComputerScore(logic.getComputerScore() + logic.getTurnTotal())
        computerScoreLabel.stringValue = String(NSLocalizedString("Computer Score: ", comment: "") + String(logic.getComputerScore()))
    }
    
    private func changeTurn() {
        logic.setIsPlayerTurn(!logic.getIsPlayerTurn())
        setButtonStates()
        if !logic.getIsPlayerTurn() {
            computerTurn()
        }
    }
    
    private func setButtonStates() {
        holdButton.enabled = logic.getIsPlayerTurn()
        rollButton.enabled = logic.getIsPlayerTurn()
        newGameButton.enabled = logic.getIsPlayerTurn()
        viewScoresButton.enabled = logic.getIsPlayerTurn()
    }

    func setImage(rollValue: Int) {
        switch rollValue {
        case 1: diceImageView.image = NSImage(named: "dice1")
        case 2: diceImageView.image = NSImage(named: "dice2")
        case 3: diceImageView.image = NSImage(named: "dice3")
        case 4: diceImageView.image = NSImage(named: "dice4")
        case 5: diceImageView.image = NSImage(named: "dice5")
        case 6: diceImageView.image = NSImage(named: "dice6")
        default: diceImageView.image = NSImage(named: "default")
        }
    }
    
    private func reset() {
        logic.setTurnTotal(0)
        logic.setPlayerScore(0)
        logic.setComputerScore(0)
        turnTotalLabel.stringValue = String(NSLocalizedString("Turn Total : ", comment: "") + String(logic.getTurnTotal()))
        playerScoreLabel.stringValue = String(logic.getPlayerName() + NSLocalizedString(" Score : ", comment: "") + String(logic.getPlayerScore()))
        computerScoreLabel.stringValue = String(NSLocalizedString("Computer Score: ", comment: "") + String(logic.getComputerScore()))
    }

}

