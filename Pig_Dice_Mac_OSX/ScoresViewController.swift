//
//  ScoresViewController.swift
//  Pig_Dice_Mac_OSX
//
//  Created by Wesley Weitzel on 7/18/16.
//  Copyright © 2016 Wesley Weitzel. All rights reserved.
//

import Cocoa
import AppKit

class ScoresViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate {
    
    @IBOutlet weak var tableView: NSTableView!
    
    var gameScores = [NSManagedObject]()
    var selectedScore: NSManagedObject?
    var managedContext: NSManagedObjectContext?
    
    private let entityName = "Scores"
    
    @IBAction func dismiss(sender: AnyObject) {
        self.dismissController(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.setDelegate(self)
        tableView.setDataSource(self)
        fetchScores()
    }
    
    func fetchScores() {
        let fetchRequest = NSFetchRequest(entityName: entityName)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "playerScore", ascending: false)]
        // Initialize Asynchronous Fetch Request
        let asynchronousFetchRequest = NSAsynchronousFetchRequest(fetchRequest: fetchRequest) { (asynchronousFetchResult) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.processAsynchronousFetchResult(asynchronousFetchResult)
            })
        }
        let appDelegate = NSApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        do {
            // Execute Asynchronous Fetch Request
            let asynchronousFetchResult = try managedContext.executeRequest(asynchronousFetchRequest)
            print(asynchronousFetchResult)
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.userInfo)")
        }
    }

    func processAsynchronousFetchResult(asynchronousFetchResult: NSAsynchronousFetchResult) {
        if let result = asynchronousFetchResult.finalResult {
            // Update gameScores
            gameScores = result as! [NSManagedObject]
            // Reload Table View
            tableView.reloadData()
        }
    }
    
    func numberOfRowsInTableView(tableView: NSTableView) -> Int {
        return gameScores.count
    }
    
    func tableView(tableView: NSTableView, viewForTableColumn tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let result = tableView.makeViewWithIdentifier(tableColumn!.identifier, owner: self) as! NSTableCellView
        let score = gameScores[row]
        if let val = score.valueForKey(tableColumn!.identifier) as? String {
            result.textField?.stringValue = val
        } else {
            result.textField?.stringValue = ""
        }
        return result
    }
    
    private let entity = "Scores"
    @IBAction func clearData(sender: NSButton) {
        deleteAllData(entity)
    }
    
    func deleteAllData(entity: String)
    {
        let appDelegate = NSApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.deleteObject(managedObjectData)
            }
        } catch let error as NSError {
            print("Delete all data in \(entity) error : \(error) \(error.userInfo)")
        }
        do {
            try managedContext.save()
            fetchScores()
        } catch {
            // Do something in response to error condition
        }
    }
    
}

